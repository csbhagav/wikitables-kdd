\section{\systemNameHeader}

In this section, we describe our \systemName{} system, and its \tableSearchTaskName{} and 
\columnRankingTaskName{} capabilities. 

\systemName{} performs the \tableSearchTaskName{}  and \columnRankingTaskName{} tasks
using a corpus of tables extracted in advance from Wikipedia. Parsing Wikipedia tables accurately requires 
some care. We first describe our extraction method in detail and evaluate its accuracy. 
We then describe how \systemName{} utilizes the extracted tables for the \tableSearchTaskName{} 
and \columnRankingTaskName{} tasks.

\subsection{Extracting Wikipedia Tables}
\label{tableExtr}
Wikipedia offers free downloads of its content. The data-source for
\systemName{} is the English Wikipedia XML dump available online.\footnote{\url{http://dumps.wikimedia.org/enwiki/}}
The dump contains data in Wiki Markup which encases tables between the `\{|' and `|\}'
tags.\footnote{\url{http://en.wikipedia.org/wiki/Help:Wiki_markup}} 
However, Wikipedia pages may also contain tables that are \emph{not} found within the
`\{|' and `|\}' table tags. Wikipedia renders these tables to Web users 
at runtime by expanding \emph{templates}.\footnote{\url{http://en.wikipedia.org/wiki/Help:Template}} 

Parsing templates is complicated as they can be nested or can take
arguments.
Rather than implementing a template parser, we used the Wikipedia
API\footnote{\url{http://en.wikipedia.org/w/api.php}} to obtain an article's
content in HTML format. To minimize the number of API calls, we first scanned
the XML dump and extracted a list of pages that either contained tables (i.e.
within `\{|' and `|\}' tags) or contained templates that expand to form tables.
In this way, we found nearly 643,000 Wikipedia pages that contained tables.
 
\systemName{} scans the HTML content of a page obtained via Wikipedia APIs  
and extracts {\tt <table>} elements 
having a \emph{class} attribute of {\tt wikitable}. 
We restrict extraction to only the {\tt wikitable} class since it is 
the only class that denotes data tables on Wikipedia.\footnote{\url{http://en.wikipedia.org/wiki/Help:Wikitable}} 
There are other parts 
of a page that are rendered as tables -- such as infoboxes, table of contents or meta-data boxes --
which we do not wish to include in our table corpus. Filtering
on the class attribute allows us to ignore such tables.

Wikipedia tables have a somewhat complicated structure due to extensive use of column and row spans. We handle 
row spans and column spans by duplicating the cell which has a rowspan or colspan value greater than 1. 
% Figure \ref{normalizeFig} illustrates how a rowspan=2 is handled by duplicating the table cell's  
% value.
In this way, we normalize each table into an $m \times n$ (m rows, n columns) matrix of cells. 
Each {\tt <td>}
element of the table is processed to remove extraneous elements (such as images), which we do
not utilize for the \columnRankingTaskName{} and \tableSearchTaskName{} tasks, and to extract
text and the list of Wikipedia links contained in each table cell. This extracted data is stored 
in corresponding \emph{matrix cells}. The resulting matrices form our corpus of extracted
normalized tables, $\tableCorpus{}$, which contains approximately 1.4 million tables.

To evaluate the quality of our table extraction, we picked 50 random pages (using
Wikipedia's random page generator\footnote{\url{http://en.wikipedia.org/wiki/Special:Random}}) that contained
 at least 1 table. The total number of tables found on these 50 pages was 111.
 \systemName{} extracted 82 out of these 111 tables ($recall = 0.74$). 
 The total number of cells in the 82 tables is 6404, of which
\systemName{} extracted 6383 cells accurately $\left(precision = 99.7\%\right)$.
This is much more precise than table extractors that crawl the full Web, which are
faced with lower-quality content and the challenging problem of interpreting 
when {\tt <table>} tags indicate relational data rather than page layout. In fact,
the previous WebTables system achieved a precision of only 41\% in extracting relational data tables  \cite{cafarella2008uncovering},
whereas all tables extracted by \systemName{} contain relational data.  

Our analysis shows that the vast majority of \systemName{}'s missing recall is due to 
incomplete annotation within Wikipedia. Of the 29 tables not found on \systemName{}, 27 tables did not have the class attribute {\tt wikitable}, though they 
contained tabular data. The remaining 2 tables were added to some pages between the time we extracted 
tables and the time we performed this experiment. 
The test set for this experiment with manual annotations is provided online.

Hence, we find that \systemName{} extracts tables at very high precision at the
cost of some recall. As described above, most of the loss in recall is due to
incomplete annotations by editors. Identifying and fixing such errors is part of
future work. Our experiments in Section \ref{section:expts} show that this level
of extraction precision and recall is sufficient to build a system that is
better than other implementations of \tableSearchTaskName{}.

\subsection{\tableSearchTaskNameHeader{} Implementation}
\label{section:tsTask}
The dataset used for \tableSearchTaskName{} is the set of normalized tables,
$\tableCorpus{}$, extracted from Wikipedia.
Our goal is to find the most relevant tables from $\tableCorpus{}$ for a given
textual query $q$, using contents of the table and the text around the table.
To incorporate textual features of the page containing the table, we use an existing search
service.\footnote{\url{http://developer.yahoo.com/boss/search/}} At runtime, a
user's query is first sent to the search service, and result URLs restricted to the
\emph{en.wikipedia.org} domain are retrieved. Using $\tableCorpus{}$, we 
obtain tables found in each of the top 30 Wikipedia pages returned for a query.
A trained linear ranking model (learned with Coordinate Ascent \cite{metzler2007linear})
ranks these tables using four types of features described in Table
\ref{tableSearchFeatures}.  Three types of features are query independent:
\emph{Page Features} capture reputability of a page, \emph{Table Features}
indicate the size and completeness of a table, and \emph{Table+Page Features}
measure the importance of a table relative to other tables on the same page.
Finally, \emph{Query Features} consist of features that are query dependent and capture
how well the query matches the table and page.  We train our ranker on a small set of hand-labeled examples,
as described in Section \ref{section:ts:datasets}.

\begin{table}[!ht]
\centering
\scriptsize{
\begin{tabular}{|p{2.3cm}|p{5.4cm}|}
\hline
\textbf{\emph{Name}}   &   \textbf{\emph{Description}} \\ \hline
\multicolumn{2}{|l|}{\textbf{\emph{Page Features}}} \\ \hline
yRank	&	Rank in Yahoo!'s results for query \\
inLinks &   Number of in-links to page\\
outLinks    &   Number of out-links from\\
pageViews   & Number of page views\\ \hline
\multicolumn{2}{|l|}{\textbf{\emph{Table Features}}} \\ \hline
numRows &   Number of rows \\ 
numCols &   Number of columns\\ 
emptyCellRatio   & Fraction of empty cells\\ \hline
\multicolumn{2}{|l|}{\textbf{\emph{Table+Page Features}}} \\ \hline
sectionNumber   &   Wikipedia Section index the table occurs in\\ 
tableImportance &   Inverse of number of tables on page \\ 
tablePageFraction   &   Ratio of table size to page size    \\ \hline
\multicolumn{2}{|l|}{\textbf{\emph{Query Features}}} \\ \hline
qInPgTitle  &   Ratio: Number of query tokens found in page title \textbf{to} total number of tokens  \\
qInTableTitle &   Ratio: Number of query tokens found in table title \textbf{to} total number of tokens      \\ \hline
\end{tabular}
}
\caption{Features used by \systemName{} for the \tableSearchTaskName{} task}
\label{tableSearchFeatures}
\end{table}

\subsection{\columnRankingTaskNameHeader{}  Implementation}
\label{exp:relJoin}
In this section we describe how \systemName{} performs the \columnRankingTaskName{} task.
\systemName{}'s implementation of the \columnRankingTaskName{} task 
requires identifying \emph{CandidateColumns} and computing Semantic Relatedness (SR) for
the candidates in real-time.  We begin by describing pre-processing which
enables efficient execution at query time.

\subsubsection{Pre-Processing}
Our table corpus $\tableCorpus{}$, contains more than 7.5 million columns in all
the tables in $\tableCorpus{}$ which makes finding joins at runtime
prohibitively inefficient.
In order to find relevant joins in realtime, we pre-process tables in $\tableCorpus{}$ to identify which tables
can be joined with a given table.
This pre-processing allows \systemName{} to quickly identify
\emph{CandidateColumns} for a given query table efficiently, and then use
machine learning models (described in the following section) to rank these
\emph{CandidateColumns}.

We pre-compute pairs of matched columns, $(\mathbf{c}^{i}_{s},\mathbf{c}^{j}_{m})$ for columns 
$\mathbf{c}^{i}_{s} \in \mathbf{T}_{i}$ and $\mathbf{c}^{j}_{m} \in \mathbf{T}_{j}$. Here
$\mathbf{c}^{i}_{s}$ is the \emph{SourceColumn} and $\mathbf{c}^{j}_{m}$  
is the \emph{MatchedColumn}. The percentage of values in $\mathbf{c}^{i}_{s}$ found in $\mathbf{c}^{j}_{m}$
is called the \emph{MatchPercentage}. It must be noted that \emph{MatchPercentage} is not a commuative property
of a pair of columns, $(\mathbf{c}^{i}_{s},\mathbf{c}^{j}_{m})$. 

To reduce the number of columns that are considered as candidates for joins, we
use heuristics to select only those columns that 
\begin{inparaenum}[(i)]
  \itemsep-0.5em 
  \item are non-numeric
  \item have more than 4 rows of data
  \item have an average string length greater than 4.
\end{inparaenum}
This prevents joining tables on columns 
that contain serial numbers, ranks, etc. In our experience, 
using these heuristics improves the quality of joins dramatically.

After filtering out columns with the heuristics, we are left with about
1.75M columns. We compute these matches exhaustively, to 
obtain the \emph{MatchPercentage} for each pair of columns. 
Matches that have a \emph{MatchPercentage} greater than 50\% are added to
our corpus of matches $\matchCorpus{}$.  

\input{include/algo.tex}

We have nearly 340 million matches in the set, $\matchCorpus{}$, of matching pairs.
Section \ref{sec:frj} explains how $\tableCorpus{}$ and $\matchCorpus{}$ are
used at runtime to find candidate columns to add to a table and rank them by
relevance.

\subsubsection{Semantic Relatedness in \systemName{}}

A {\em Semantic Relatedness} (SR) measure takes as input two
concepts, and returns a numeric relatedness score for the concept pair.
Our experiments demonstrate that preferring joins between page concepts
with higher SR results in significant performance improvements on the 
\columnRankingTaskName{} task. 

In \systemName{}, we use the $MilneWitten$ Semantic Relatedness measure \cite{witten2008effective}, 
which estimates the relatedness of Wikipedia concepts (each Wikipedia page is treated as a 
``concept'').  $MilneWitten$ is based on the intuition that more similar Wikipedia pages should
share a larger fraction of inlinks.  We use the $MilneWitten$ implementation from 
Hecht et al. \cite{hecht2012explanatory}, an enhancement to the original
measure that emphasizes prominent links within the Wikipedia page ``gloss'', and
learns parameters of the measure based on hand-labeled relatedness judgments.

Utilizing SR at runtime within \systemName{} requires quickly computing the relatedness
between the page containing the query table and all other pages containing candidate
columns.  Naively computing in-link intersections with every candidate column at query time would
be intractable.  To solve this performance issue, we pre-compute all non-zero relatedness scores 
for all concepts on Wikipedia, and store these in memory.  The data store is
compressed (by quantizing SR scores to 256 bins and using simple variable-byte encoding),
and occupies about 30GB of memory.  We have provided access to this data store through a public 
API.\footnote{\url{http://downey-n2.cs.northwestern.edu:8080/wikisr/sr/sID/69058/langID/1}}

\subsubsection{Finding Relevant Joins}
\label{sec:frj}
In \systemName{}, the user begins by selecting a table to view.  This
table is the input to the 
$\mathbf{\textsc{FindRelevantJoins}}$ algorithm illustrated in Algorithm
\ref{alg:findRelevantJoins}.
% \textbf{Algorithm Walk Through:}
$\mathbf{\textsc{FindRelevantJoins}}$ takes a query table
$\queryTable{}$ and the table corpus $\tableCorpus{}$ as inputs, and returns a set of triplets.
A triplet contains a \emph{SourceColumn}, a \emph{MatchedColumn} and
a \emph{CandidateColumn}, denoted $\left(\mathbf{c}^{q}_{s}, \mathbf{c}^{t}_{m},
\mathbf{c}^{t}_{c}\right)$. These triplets are ranked on the basis of the
estimated relevance of adding $\mathbf{c}^{t}_{c}$ to $\queryTable{}$. From the top ranked triplet,
$\mathbf{c}^{t}_{c}$ can be added to $\queryTable{}$ through a left outer join between $\queryTable{}$
and $\mathbf{T}_{t}$ ON $\mathbf{c}^{q}_{s} = \mathbf{c}^{t}_{m}$.

These triplets are formed by first getting all matches from $\matchCorpus{}$, 
in decreasing order of \emph{MatchPercentage} such that 
one of the columns from $\queryTable{}$ is the 
\emph{SourceColumn} (the function \textsc{GetMatchPairs} in Algorithm
\ref{alg:findRelevantJoins}). All columns from 
the matched column's table except the matching column are considered as candidate columns. 

Candidate columns are first classified as either \emph{relevant} or \emph{non-relevant} 
using a trained classifier model, and then ranked using a ranking model. The classifier 
discards low quality column additions. We used the Weka implementation \cite{hall2009weka} 
of the Logistic Regression model as
our classifier, and a linear feature based Coordinate Ascent ranking model, implemented by RankLib.\footnote{\url{http://people.cs.umass.edu/~vdang/ranklib.html}}  
Both models use features listed in Table \ref{columnRankerFeatures}.
The models were trained on small set of hand labeled examples as described
in Section \ref{sec:reljoindatasets}.

Entities in the \emph{SourceColumn} may be present more than once in the
\emph{MatchedColumn}. In this case, all values from the \emph{AddedColumn} that
correspond to a single entity in the \emph{MatchedColumn} are grouped together
and added to the query table, i.e. we display all values that we join to within
a single cell.  A feature 
(``avgNumKeyMap'' in Table \ref{columnRankerFeatures}) measures how many
values each cell in the added column contains, on average.  Our results
(Section \ref{section:rj}) show that one-to-many joins are not preferred by
users---there is a strong negative
correlation between the avgNumKeyMap feature and the relevance rating of a column.

\begin{table}[h]
\centering
\scriptsize{
\begin{tabular}{|p{2.6cm}|p{5.2cm}|}
\hline
\textbf{\emph{Name}}   &   \textbf{\emph{Description}} \\ \hline
matchPercentage     &     \emph{MatchPercentage} between \emph{SourceColumn} and \emph{MatchedColumn} \\ \hline
avgNumKeyMap	&	Average number of times a key in the \emph{SourceColumn} is found in the \emph{MatchedColumn} \\ \hline
srRelate	& SR measure between the page containing the \emph{SourceColumn} and the page containing the \emph{MatchedColumn}  \\ \hline
srcAvgLen	&	Average length of data in the \emph{SourceColumn} \\ \hline
srcDistinctValFrac	&	Fraction of distinct values in the \emph{SourceColumn}	 \\ \hline
srcNumericValFrac	& Fraction of columns in \emph{SourceTable} that are numeric  \\ \hline 
tgtAvgLen	&	Average length of data in the \emph{CandidateColumn}	 \\ \hline
tgtIsNum	&	Boolean, 1 if \emph{CandidateColumn} is numeric	 \\ \hline 
tgtDistinctValFrac	&	Fraction of distinct values in the \emph{CandidateColumn}	 \\ \hline
inLink	& Number of inlinks to the page to which \emph{TargetTable} belongs	\\ \hline
outLink	& Number of outlinks from the page to which \emph{TargetTable} belongs	\\ \hline
srcTableColIdx	&	Boolean feature: 1 if \emph{SourceColumn} is among the first three columns of the table	\\ \hline 
matchedTableColIdx	&	Boolean feature: 1 if \emph{TargetColumn} is among the first three columns of the table	\\ \hline
\end{tabular}
}
\caption{Features used by \systemName{} for the \columnRankingTaskName{} task}
\label{columnRankerFeatures}
\end{table}