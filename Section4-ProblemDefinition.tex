\section{Problem Definition}
In this section, we formally define our two information retrieval tasks: \tableSearchTaskName{} and \columnRankingTaskName{}.
Both tasks utilize a corpus of tables extracted from Wikipedia, so we begin by formally defining the table extraction
task.

\subsection{Table Extraction from Wikipedia}
\systemName{} scans all Wikipedia articles for tables. Each table found in an article is extracted and converted 
into an $m \times n$ matrix of cells (details in Section \ref{tableExtr}). We refer to these 
matrices as \emph{normalized tables}. Each cell in the matrix 
holds data from one cell in the table, 
consisting of all string values and 
links to Wikipedia concepts contained in the table cell. 

The set of all normalized tables extracted from Wikipedia forms our corpus, $\tableCorpus{}$. 
Each normalized table $\mathbf{T}_{i} \in \tableCorpus$, having $m$ rows and $n$ 
columns is represented as a list of column vectors:
\begin{align*}
\mathbf{T}_{i} = (\mathbf{c}^{i}_{1} & \mathbf{c}^{i}_{2} \ldots \mathbf{c}^{i}_{n}) \\
& \text{where } \forall k \in \left[ 1,n \right]  \ \mathbf{c}^{i}_{k} \text{ is a vector of length } m   
\end{align*}

\subsection{\tableSearchTaskNameHeader{}}
{\em Table search} is the task of returning a ranked list of tables for a
given textual query. Formally, $\mathbf{TableSearch(}$$q$$\mathbf{,
\tableCorpus{})}$ takes as input a query, $q$, and the corpus of tables,
$\tableCorpus{}$ and returns a list of tables in decreasing order of relevance
to $q$. For example, the top three results returned by \systemName{} for
$\mathbf{TableSearch(}$\emph{$\tt{country\;population}$}$\mathbf{ , \tableCorpus{})}$
are \begin{inparaenum}[\itshape 1\upshape)]
  \item List of countries by population
  \item List of sovereign states and dependent territories by population density
  \item World population.\footnote{\url{http://downey-n1.cs.northwestern.edu/tableSearch/search.html?q=country+population}} 
\end{inparaenum}

\subsection{\columnRankingTaskNameHeader{}}
\input{include/Screenshot.tex}
% \vspace{-0.1cm}
{\em Relevant join} is the task of finding \emph{columns} that can be added to a given table and ranking 
them in descending order of relevance to the given table.
 
Consider a user viewing table $\queryTable{}$, the \emph{QueryTable}, as shown in 
Figure \ref{termExplain}.
An informative column to add to this table could be the population data of each
country.
This data exists in a different table, denoted as $\targetTable{}$
(the \emph{TargetTable}). The ``Country'' column of $\queryTable{}$ and the
``Nation'' column of  $\targetTable{}$ contain nearly identical data, which
suggests the two tables can be joined on these columns. The columns from
$\queryTable{}$ and $\targetTable{}$ that contain similar data are called
the \emph{SourceColumn} ($\sourceCol{}$) and \emph{MatchedColumn}
($\matchedCol{}$) respectively. 
All columns from $\targetTable{}$ except $\matchedCol{}$ can be added to
$\queryTable{}$. We refer to any column that can be added to a \emph{QueryTable} from a \emph{TargetTable} as
a \emph{CandidateColumn}, denoted by $\addableCol{}$. The final table,
$\finalTable{}$, is created by appending $\queryTable{}$ with the ``Population''
column from $\targetTable{}$. A \emph{CandidateColumn} that is added to a \emph{QueryTable}
is referred to as an \emph{AddedColumn}, $\addedCol{}$.
 
Formally, 
$\textsc{FindRelevantJoin}(\queryTable{} , \tableCorpus{})$ takes a query table $\queryTable{}$ and 
the corpus of tables $\tableCorpus{}$ as inputs, and returns a ranked list of 
triplets denoting relevant joins. Each triplet consists of the \emph{SourceColumn} ($\mathbf{c}^{q}_{s}$), 
the \emph{MatchedColumn} ($\mathbf{c}^{t}_{m}$) and the \emph{CandidateColumn} ($\mathbf{c}^{t}_{c}$).
Triplets $(\mathbf{c}^{q}_{s} , \mathbf{c}^{t}_{m} , \mathbf{c}^{t}_{c})$ are ranked in 
decreasing order of relevance of $\mathbf{c}^{t}_{c}$ to $\queryTable{}$. In each triplet, $m \neq c$, columns 
$\mathbf{c}^{t}_{m} , \mathbf{c}^{t}_{c} \in \mathbf{T}_t$ and $\mathbf{c}^{q}_{s} \in \queryTable{}$. 
Columns  $\mathbf{c}^{q}_{s}$ and $\mathbf{c}^{t}_{m}$ are chosen such that they contain nearly identical data 
in their cells. 

As a concrete example, Figure \ref{fig:screenshot} shows a table from the
\emph{``List of countries by GDP (nominal)''} Wikipedia page.\footnote{\url{http://en.wikipedia.org/wiki/List_of_countries_by_GDP\_(nominal)#List}}
This table is the query table, $\queryTable{}$. The column \emph{``GDP (PPP) \$Billion''} is the \emph{AddedColumn} $\mathbf{c}^{t}_{a} \in
\mathbf{T}_{t}$, where $\mathbf{T}_{t}$ is the \emph{``List of countries by GDP (PPP)''}
table.\footnote{\url{http://en.wikipedia.org/wiki/List_of_countries_by_GDP_(PPP)#Lists}}
The data in the ``Country/Region'' column in $\queryTable{}$ matches the data from the
``Country'' column in $\mathbf{T}_{t}$.
These two columns from $\queryTable{}$ and $\textbf{T}_t$ are the match
columns $\mathbf{c}^{q}_{s}$ and $\mathbf{c}^{t}_{m}$ respectively.

The \columnRankingTaskName{} task involves two primary challenges.  First, we wish to select an 
\emph{AddedColumn} that lists attribute values for the entities in the \emph{SourceColumn}.  In practice this can
be challenging.  Consider a hypothetical table containing columns, ``City,'' ``Country,'' and ``Mayor.''
Here, ``Mayor'' is a property of ``City,'' and not of ``Country.''  Thus, if we erroneously select ``Country'' as a 
\emph{MatchedColumn} and ``Mayor'' as an {\em AddedColumn}, this will lead to a poor join.
Secondly, we wish to choose \emph{AddedColumns} that are relevant to the query table.  For example,
the ``prisoners'' column is relevant to the incarceration table in Figure \ref{fig:screenshot}, whereas other
attributes of countries (Olympic gold medal winners, for example) are far less relevant.  The \columnRankingTaskName{}
task requires identifying this distinction automatically, for a given table.
Both of the above challenges are addressed by our trained models for the \columnRankingTaskName{}, which are described
in the following section.
 
 
 
 