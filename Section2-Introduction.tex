\section{Introduction}

The Web contains a multitude of data tables, and recent years have seen increasing interest in extracting 
this data to power new Web search applications \cite{GatterbauerBHKP2007:Towards,chan2008vispedia,cafarella2008webtables,chakrabarti2010enhancing,venetis2011recovering,sarmad2012finding}.

In this paper, we focus on extracting table data for two information retrieval tasks.  
The first is \tableSearchTaskName{}, the task of returning relevant tables for a given 
textual query \cite{cafarella2008webtables,venetis2011recovering}.  For example, consider a
Web user investigating $\tt{world\;electricity\;use}$.  Given this query, a
table search system might return the ``electricity consumption'' table from Wikipedia (which lists 
total Megawatt-hours consumed for each country, and other data).%\footnote{\url{http://en.wikipedia.org/wiki/List_of_countries_by_electricity_consumption}}

At this point, the user may be curious how other properties of countries (e.g. CO2 emissions, GDP, etc.) are 
related to electricity consumption.
This information need motivates our second task, \columnRankingTaskName{}: the novel task of automatically 
identifying, given a query table $\queryTable{}$, which columns from other tables would make 
relevant additions to $\queryTable{}$.  Viewed as a database join, in the \columnRankingTaskName{} task
we first identify a column $c$ in $\queryTable{}$ to use for joining---e.g., country names in the electricity
consumption table.  Then, we find a distinct table $\targetTable{}$ (e.g. a list of countries by GDP) with a column $c'$ similar to $c$,
and perform a left outer join on $c=c'$ to augment $\queryTable{}$ with an automatically selected target 
column from $\targetTable{}$ (e.g., the GDP amounts).
Unlike a standard database join, the \columnRankingTaskName{} task requires
automatically selecting the join columns and the target column to make a {\em relevant}
addition to the query table.
Automatically joining distinct tables can provide users with unified
data views that aren't available in any single table on the Web.  In our example, the user might
learn that electricity consumption is strongly correlated with GDP, and even more so
with population.

As argued in Section \ref{sec:previous}, table extraction offers certain advantages over other Web information
extraction techniques (e.g., extraction from unstructured text \cite{etzioni2004web, pasca2006organizing, banko-ijcai07,bunescu-acl07,Fang:2011:SPR:1935826.1935933}).  However, table extractors 
also face unique challenges.  First, table extractors must automatically
distinguish when HTML {\tt <table>} tags indicate relational
data, rather than page layout.  Further,
Web tables are of widely varying quality, and can have opaque semantics: 
header rows are often missing or uninformative,
and the meaning of a table can be buried in text surrounding the table \cite{venetis2011recovering}.  
These factors make the \tableSearchTaskName{}
and \columnRankingTaskName{} tasks challenging on the Web.

In this paper, we introduce \systemName{}, a system that leverages Wikipedia tables and
machine learning methods to perform the \tableSearchTaskName{} and \columnRankingTaskName{} tasks.
\systemName{} has two key characteristics:
\begin{enumerate}
\item \systemName{} relies on tables from Wikipedia.  Our results indicate that Wikipedia's smaller size (1.4M data tables, compared to an estimated 154M across
the Web \cite{cafarella2008webtables}) is outweighed by its high quality
and more easily extracted semantics.  In contrast to Web tables, Wikipedia's tables are explicitly distinguished from page layout, and 
rarely duplicated across pages.
\item \systemName{} leverages Wikipedia's rich content and link structure as 
features within machine learners to perform the \tableSearchTaskName{} and \columnRankingTaskName{} tasks. 
As one example, the semantic relatedness (SR) between Wikipedia pages estimated from the link graph
\cite{witten2008effective} forms an extremely valuable feature for identifying relevant joins.
\end{enumerate}

Our experiments show that mining Wikipedia-specific information enables 
\systemName{} to be effective in the \tableSearchTaskName{}
and \columnRankingTaskName{} tasks.  On existing benchmark queries for
\tableSearchTaskName{}, \systemName{} outperforms the state-of-the-art Google Fusion 
Tables system \cite{venetis2011recovering}, {\em despite} 
using an order of magnitude fewer tables.  Our analysis reveals that Fusion Tables does 
offer significantly broader coverage for certain types of queries (e.g. commercial queries and large entity sets), suggesting
that an important item of future work is to combine both systems to maximize precision and recall.
On the \columnRankingTaskName{} task, we show that \systemName{} more than doubles the accuracy
of baselines, and that SR measures account for 58\% of the increase.

Our results suggest that table extraction applications, even across the broader
Web, would benefit by including special-purpose features and models for
Wikipedia’s rich content.
Lastly, our work includes the release of several resources for table extraction,
including over 15 million tuples of extracted tabular data, the first manually
annotated test sets for the \tableSearchTaskName{} and \columnRankingTaskName{}
tasks, and public APIs for accessing tabular data and computing Semantic
Relatedness.\footnote{\url{http://downey-n1.cs.northwestern.edu/public/}}