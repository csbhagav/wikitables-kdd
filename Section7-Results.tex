\section{Experiments}
\label{section:expts}
In this section, we present our evaluation of \systemName{} on the
\tableSearchTaskName{} and \columnRankingTaskName{} tasks.
We describe the datasets, comparison methods, and metrics used for this
evaluation, along with results obtained. We also present an error analysis and
an analysis of the utility of the different features used for each task.
The experiments show that
\systemName{} outperforms current state-of-the-art \tableSearchTaskName{}
methods, and that \systemName{} beats baseline methods on the
\columnRankingTaskName{} task.
We also demonstrate that Semantic Relatedness measures defined over
Wikipedia concepts are especially valuable for the \columnRankingTaskName{} task.

\subsection{\tableSearchTaskNameHeader{}}
%Here we describe our evaluation of \systemName{} 
%on the \tableSearchTaskName{} task. 
We compare \systemName{} with two systems \begin{inparaenum}[(i)]
\item previously published work by \venetisEtAl{} \cite{venetis2011recovering}, and
\item a publicly available state-of-the-art table search system, \googft{}.
\end{inparaenum}
We define metrics that are used to measure relative performance
of table search methods and present results that show
that \systemName{} outperforms \ventisSys{} in the \tableSearchTaskName{} task.
The results of the \tableSearchTaskName{} experiments also show that even with a 
much smaller dataset of tables, \systemName{} satisfies more user queries 
aimed at retrieving tables from the Web.  

\subsubsection{Datasets}
\label{section:ts:datasets}
As we compare \systemName{} with Web-based extractors, 
it is important that we do not bias the evaluation in favor of information from Wikipedia. 
To prevent this bias, all of our \tableSearchTaskName{} evaluation query workloads 
are drawn from previous work on searching tables from the Web at large.

Our first test set $\tt{D1}$ consists of a set of 100 queries of the form
$(C,P)$, where $C$ is a string that denotes a \emph{class} of instances, and $P$
is also a string that denotes a \emph{property} associated with these instances.
This dataset is taken from previous work by \venetisEtAl{}
\cite{venetis2011recovering}.
For each query in the test set of 100 $(C,P)$ pairs, \venetisEtAl{} rated the
top 5 tables retrieved by a table search method as either \emph{right on} (if it
had all information about a large number of instances of the class and values
for the property), \emph{relevant} (if it had information about only some of the
instances, or of properties that were closely related to the queried property)
or \emph{irrelevant}. Some systems of table search 
(e.g. \textsc{Goog} , \textsc{GoogR} - described in the following section),
used by \cite{venetis2011recovering} to compare with \ventisSys{}, 
return documents and not just tables. Thus,
annotators also annotated if a result was found \emph{in a table}. On the other hand,
\systemName{} only returns tables as results of a search query. 

We replicated this evaluation method on \systemName{}, hand-labeling results for
each of the 100 queries. 
Because \systemName{} takes a single string as its input rather than a $(C,P)$
pair, we simply concatenate the class and property strings to form the textual
query for our system. More sophisticated handling of classes and properties
within \systemName{} is an item of future work.

%  For our evaluation, two annotators
% annotated results for all  Annotator agreement for this
% task, measured as the Spearman's rank correlation coefficient between ratings of
% annotators on retrieved tables for all queries, was found to be 0.73.

Our second test set $\tt{D2}$ consists of 8 textual queries drawn from
previous work by \sarmaEtAl{} \cite{sarmad2012finding}. We use these queries to 
compare \systemName{} with \googft{}.  Two raters 
annotated each table returned for queries in $\tt{D2}$ on a scale of 0 to 5. The average Spearman's correlation coefficient 
between ratings of the annotators across all queries was 0.71, which we believe is sufficiently
high for such a subjective task.  

For training the \systemName{}'s table search ranker (Section \ref{section:tsTask}),
we utilized a training set of about 350 labeled examples on 5 queries, disjoint from the
test set queries.

\subsubsection{Comparison Methods}
\label{ts:compareMethods}
We compare \systemName{} with multiple Web table search systems from previous work.
Our primary evaluation compares performance on data set $\tt{D1}$ with previously
published results from \cite{venetis2011recovering}.  The best-performing
method from that work, \ventisSys{}, automatically annotates all 
tables in the corpus with labels, which 
indicate what a table is \emph{about}, and ranks these labels on importance.
The table search system within \ventisSys{} takes queries of the form $(C,P)$,
where $C$ is a class name and $P$ is a property, and returns a ranked list of tables.  
To create this ranked list, \ventisSys{} considers tables in the corpus 
that have a class label $C$ in the top-$k$ class labels in its annotations. \ventisSys{}
then ranks these tables based on a weighted sum of a number of signals, some of which 
are derived from the property $P$ from the query. The weights were determined
using a training set of examples. In their published results, 
\venetisEtAl{} compared performance of \ventisSys{} with three other methods:
 \begin{inparaenum}
\item \textsc{Goog} : the results returned by www.google.com, 
\item \textsc{GoogR}: the intersection of the table corpus (from \ventisSys{}) with the top-1,000 results returned by GOOG,
and 
\item \textsc{Document}: document-based approach proposed in \cite{cafarella2008webtables}. 
\end{inparaenum}
We include results of the performance of all methods compared in \cite{venetis2011recovering}, for completeness.

\begin{table*}[!ht]
\centering
\scriptsize{
\begin{tabular}{||l||c|ccc||p{0.7cm}|ccc||ccc||}
\hline
Method  &   \multicolumn{4}{c||}{All Ratings}   &   \multicolumn{4}{c||}{Ratings by Queries}	&   \multicolumn{3}{c||}{Metrics}    \\ \hline
    &   Total   &   (a) &   (b) &   (c) &   Some Result &   (a) &   (b) &   (c) 	&	$\mathbf{P_{tq}}$	&	$\mathbf{R_{tq}}$	&	$\mathbf{F1_{tq}}$	\\ \hline
\systemName{}   &   500 &   54  &   88  &   142 &   100 &   \textbf{35} &   \textbf{63} &   \textbf{63} 	&	0.63	&	\textbf{0.63}	&	\textbf{0.63}	\\ \hline   
\ventisSys{} &  175 &   69  &   98  &   93  & 49    &   24  &   41  &   40  &	\textbf{0.82}	&	0.4	&	0.54	\\ \hline
\textsc{Document}    &   399 &   24  &   58  &   47  &   93  &   13  &   36  &   32 	&	0.34	&	0.32	&	0.33	\\  \hline
\textsc{Goog}    &   493 &   63  &   116 &   52  &   100 &   32  &   52  &   35 	&	0.35	&	0.35	&	0.35	\\  \hline
\textsc{GoogR}   &   156 &   43  &   67  &   59  &   65  &   17  &   32  &   29	&	0.45	&	0.29	&	0.35  \\  \hline          
\end{tabular}
}
\caption{Comparing results of {\tt \ventisSys{}} and \systemName{}. \systemName{} outperforms {\tt \ventisSys{}} with 
a much higher recall and $F1$ score. Results of {\sc Document}, {\sc Goog} and 
{\sc GoogR} have been re-printed from \cite{venetis2011recovering}. 
The columns under All Ratings present the number of results that were
rated to be (a) \emph{right on}, (b) \emph{right on} or \emph{relevant}, 
and (c) \emph{right on} or \emph{relevant} and \emph{in a table}. The Ratings by Queries columns aggregate
ratings by queries: the sub-columns indicate the number of queries for which a table in the top 5 results 
got a rating with (a) \emph{right on}, (b) \emph{right on} or \emph{relevant}, 
and (c) \emph{right on} or \emph{relevant} and \emph{in a table}.}
\label{tab:ventisCompare}
\end{table*}

% We think that it is unnatural for a search engine to take input as a \emph{pair} of
% strings. The input query to our system is, therefore, a single string formed by concatenating
% $class$ and $property$ strings. 

% This does not give our
% implementation the advantage of knowing explicitly what the returned tables
% should be \emph{about}. \ventisSys{} also has an advantage of knowing that one of the columns 
% in the retrieved table must be related to property $P$.

In addition to comparing against previously published results, we also conducted
new experiments to compare \systemName{} with \googft{}.  \googft{} is the only publicly available 
table search system (other than \systemName{}) of which we are aware. We used
$\tt{D2}$ to compare \systemName{} with \googft{}. 
\vspace{-0.1cm}
\subsubsection{Metrics}
\label{section:ts:metric}
The primary metric we use to compare \systemName{} with previously published results on
data set $\tt{D1}$ is simply the number of queries for which a given method returned a result
in its top 5 that was {\em relevant}
or {\em right on} and {\em in a table}.  \venetisEtAl{} also evaluated {\em precision} on the
table search task \cite{venetis2011recovering}.  We consider precision to be a secondary metric for this task (it is rare in
information retrieval tasks to distinguish between returning poor results and returning no results),
but following \cite{venetis2011recovering} we also evaluate on precision and F1 metrics:\footnote{
Our metrics are similar to but differ slightly from those of \cite{venetis2011recovering};
the precise metrics they use depend on which methods retrieved accurate 
tables for which queries, which is not known to us.}

\begin{align}
&\mathbf{P_{tq}} = \frac{\text{\# queries with \emph{right on} or \emph{relevant} tables}}
                        {\text{\# queries for which results returned}}\\
&\mathbf{R_{tq}} = \frac{\text{\# queries with \emph{right on} or \emph{relevant} tables}}
                        {\text{\# queries}}\\
% &\mathbf{F1_{tq}} = \frac{2 * \mathbf{P_{tq}} * \mathbf{R_{tq}}}
%                          {\mathbf{P_{tq}} + \mathbf{R_{tq}}}
&\mathbf{F1_{tq}} = (2 * \mathbf{P_{tq}} * \mathbf{R_{tq}}) /
                          (\mathbf{P_{tq}} + \mathbf{R_{tq}})
\end{align} 

To compare \systemName{} with \googft{}, we used the standard information retrieval
metrics of Discounted Cumulative Gain
($DCG$ \cite{jarvelin2002cumulated}) and Normalized Discounted Cumulative Gain
($nDCG$). Both of these metrics are designed to give greater importance to
occurrences of more useful results higher in a ranked list. $DCG$ at a particular
rank position $p$ is given by:
\begin{align}
\mathbf{DCG_{p}} = \sum_{i=1}^{p} (2^{rel_{i}}-1)/(\log_2(i+1))
\end{align}
where $rel_{i}$ is the rating of the $i^{th}$ ranked result table. 
$nDCG$ is defined as the ratio between $DCG$ of the ranked list retrieved
and $DCG$ of the ideal ranking possible. This is given by:
\begin{align}
\mathbf{nDCG_{p}} = (DCG_{p})/(IDCG_{p})
\end{align}
Since finding the ideal ranking of all tables for a given query is difficult,
we consider the ideal ranking to be the best ranking formed from the
union of the results retrieved across all methods being evaluated.

\subsubsection{Results}
\label{ts:results}

Table \ref{tab:ventisCompare} shows the results of our experiment comparing
\systemName{} with \ventisSys{} using the Dataset $\tt{D1}$.
While \ventisSys{} only produced a result for 49
of the 100 queries, \systemName{} retrieved tables for all 100 queries.
Furthermore, \ventisSys{} returned a  \emph{right on} or \emph{relevant} table
in the top 5 results for only 40 queries, whereas \systemName{} returned a
relevant table for 63 queries---a 58\% increase.

Table \ref{tab:sarmaQueries} shows the comparison between \systemName{} and
\googft{} using Dataset $\tt{D2}$.
It lists the $DCG_{10}$ and $nDCG_{10}$ metrics of the ranked lists of tables
retrieved by \googft{} and \systemName{} for queries in the test set.
The two systems produce similar results, although \systemName{} performs
somewhat better than \googft{}.

\begin{table}[t]
\centering
\scriptsize{
\begin{tabular}{|p{2.4cm}|p{1cm}|p{1cm}|p{1cm}|p{1cm}|}
\hline
\multirow{2}{*}{\textbf{Query}} & \multicolumn{2}{|c|}{\textbf{Fusion Tables}} & \multicolumn{2}{|c|}{\textbf{\systemNameHeader{}}} \\
 & \tiny{$\mathbf{DCG}_{10}$} & \tiny{$\mathbf{nDCG_{10}}$} & \tiny{$\mathbf{DCG_{10}}$} & \tiny{$\mathbf{nDCG_{10}}$}\\ \hline
country gdp & 44.2 & 0.43 & 71.7 & 0.69 \\
country population & 51.7 & 0.5 & 76.5 & 0.74 \\
dog species & 70.2 & 0.71 & 55.6 & 0.56 \\
fish species & 47.7 & 0.69 & 26.5 & 0.39 \\
movie director & 43.1 & 0.73 & 13.3 & 0.23 \\
national parks & 60.6 & 0.74 & 52.1 & 0.64 \\
nobel prize winners & 36.4 & 0.36 & 90.1 & 0.88 \\
school ranking & 19.7 & 0.29 & 59.4 & 0.88 \\\hline
\textbf{Average} & \textbf{46.7} & \textbf{0.56}   &    \textbf{55.7} & \textbf{0.63} \\\hline
\end{tabular}
}
\caption{Comparison between \googft{} and \systemName{}. \systemName{} performs somewhat better than \googft{}.}
\label{tab:sarmaQueries}
\end{table}

\subsubsection{Analysis}

\begin{table}
\centering
\scriptsize{

\begin{tabular}{|p{2.2cm}|p{3.5cm}|}
\hline
\textbf{Feature}	&	\textbf{Correlation with rating}	\\ \hline
tablePageFraction	&	0.41	  \\ \hline
numRows	&	0.40	 	\\	 \hline
tableCaption ContainsQuery	&	0.31		\\ \hline 
sectionNumber	&	-0.19		\\ \hline
inlinks	&	-0.16	 \\ \hline
numCols	&	0.156	\\ \hline
% tableImportance	&	0.08	\\ \hline
% outlinks	&	0.079	\\ \hline
% yRank	&	-0.05	\\ \hline
% pageContainsQuery	&	-0.05	\\ \hline
% pageViews	&	-0.01	\\ \hline
% emptyCellPercent	&	0.005	\\ \hline
\end{tabular}
\caption{Spearman's rank correlation coefficient between \tableSearchTaskName{} features and user ratings on tables retrieved for text queries.
Only features with Significant correlations ($p < 0.01$) are listed.}
\label{tab:searchCorrels}
}
\end{table}

Our results show that on query workloads from previous work on Web table search, 
the higher quality of Wikipedia data combined with our machine learning
approach results in \systemName{} outperforming Web table search systems. 
The text surrounding a table is more relevant for table search when tables
are drawn from high-quality, topically-focused pages like those on Wikipedia.

Table \ref{tab:searchCorrels} lists the Spearman's rank correlation coefficients between
features and user ratings of tables. The features in bold represent
statistically significant correlations. A few observations can be drawn from these correlations: 
\begin{inparaenum}[(i)]
 \item tables that cover a larger fraction of their page tend to be better
 \item tables with more rows tend to be better, as also observed by \cite{venetis2011recovering}
 \item a query text match with a table caption is a strong indicator of relevance
 \item tables occurring in earlier {\em sections} of a Wikipedia page tend to be better
 \item tables containing more columns are better.
\end{inparaenum} 

One surprising finding is that the search engine rank (yRank) is not significantly
correlated with table result quality.  However, we note that \systemName{} begins
by selecting the top 30 results that contain tables from the search engine, so all tables come from relatively
high-ranking results.  Also surprising is that the number of inlinks has a significant
negative correlation with result quality.  Our analysis revealed that this is because prominent 
Wikipedia pages (with more inlinks) do not necessarily contain better tables. For example, 
the ``List of counties in Maryland" page, which has only 928 inlinks, contains much higher-quality
tabular data than the ``Maryland'' page that has about 29,000 inlinks. The ``List of prolific inventors''
page, which has 67 inlinks, contains much better tabular data than the ``Alexander Graham Bell'' page that has 
about 2600 inlinks.

In our evaluation of \systemName{} on Dataset {\tt D1},
there were 37 queries for which \systemName{} did not retrieve any  \emph{right on} or \emph{relevant} tables 
in the top 5 results. We analyzed the types of queries for which \systemName{} failed to retrieve relevant tables.  

 The query classes for which \systemName{} did not retrieve accurate
 tables are the ones that are open-ended, i.e. the number of entities that
 belong to these classes is very large.  Examples include \emph{movie stars},
 \emph{guitars}, \emph{football clubs}, \emph{beers}, \emph{clothes}, etc. Since
 these classes contain large numbers of entities, they are generally not
 found in just one single table. They rather tend to be sub-categorized into
 smaller groups. Such \emph{open-ended} queries accounted for 17 out of the 37 queries 
 in which \systemName{} did not retrieve any \emph{right on} or \emph{relevant} tables.
 
 The most frequently missed \emph{property} values were \emph{price},
 \emph{cost} and \emph{market share} (of e.g. clothes, laptops, beer etc.)
 \systemName{} did not retrieve accurate tables for 10 queries that contained
 one of these three properties. This can be attributed to the fact that Wikipedia is
 not particularly focused on commercial data. In fact, we found only about 380 tables
 in our corpus $\tableCorpus{}$ of 1.4M tables 
 that contained \emph{price}, \emph{cost} or \emph{market share}
 information in one of their columns.

\subsection{\columnRankingTaskNameHeader{}}
\label{section:rj}
We now present the evaluation of \systemName{} on the \columnRankingTaskName{}
task.

\subsubsection{Datasets}
\label{sec:reljoindatasets}
We evaluated \systemName{} on a dataset that consists of tables to which columns
can be added. To provide a non-trivial number of columns to rank, 
we performed a weighted random selection of 20 Wikipedia articles that contained
at least one table, where weight of a page is the number of times a column from
that page occurs in our match corpus $\matchCorpus{}$ as a \emph{SourceColumn}.
The random list of articles used for evaluation are listed here.\footnote{\url{http://downey-n1.cs.northwestern.edu/public/randomQSet.html}}

To train the machine learning models, for \columnRankingTaskName{}, described in
Section \ref{exp:relJoin}, 4 users posted 593 ratings on 82 distinct tables,
mutually exclusive from the test set.
For the classifier, all ratings of 3 and above were considered \emph{relevant}
(positive class of the classifier).
Input for the ranker is the actual rating given by the user.
For evaluation, two annotators rated the top 4 columns added to each table from
pages in the test set, on a scale of 1 to 5, indicating relevance of an added
column to the query table. Inter annotator agreement, measured
by Spearman's rank correlation coefficient between ratings of the two annotators 
for each query, was found to be 0.73.


\subsubsection{Comparison Methods}
\label{rj:compMethods}
To our knowledge, \systemName{} is the first system that finds and ranks
relevant columns that can be added to a given table. Previous work has looked
into using \emph{Schema Complement} as an indicator of relatedness of tables and
using this relatedness to improve table search \cite{sarmad2012finding}.
\googft{} allows adding columns to a table from other tables, but a user selects
the \emph{subject} column manually and also selects the column(s) to be added.
Thus, to evaluate \systemName{} on the \columnRankingTaskName{} task, we created
a baseline method (\rjBaseline{}) and compared it with our implementation,
\rjBest{}. \rjBaseline{} simply ranks \emph{CandidateColumns} in decreasing order 
of \emph{MatchPercentage}. In an ablation study to evaluate the impact of \emph{Semantic
Relatedness} on this task, we created a system (\rjAblateSR{}) which ignores the
\emph{srRelate} feature from both the classifier and ranking models. 

\subsubsection{Metrics}
We use the $nDCG$ metric introduced in Section \ref{section:ts:metric}.
The metric is calculated for the top 4 columns added by a method to each query
table. We choose to evaluate on the top 4 as this is a typical number of added
columns that fits in a single browser window.
The normalizing factor in this case is the ideal ranking of the union of columns
added by the three methods that are being compared.
Performance of each method can be estimated by taking the average of $nDCG$
values across all queries.

We also measure the accuracy of columns that are added to a table by a method. 
We define this metric as:
\begin{align}
\mathbf{Acc_{m}} = \frac{\text{\# columns with rating $\geq$ 3}}{\text{\# columns added by method m}}
\end{align}
\subsubsection{Results}
The results of our evaluation are summarized in Table \ref{table:comparison},
which lists the average accuracy of the four columns added by each method, and
the average $DCG$ and $nDCG'$ values of results retrieved for all query tables.
Both \rjBaseline{} and \rjAblateSR{} added columns to 106 tables and \rjBest{}
added columns to 103 tables.
Results show that compared to \rjBaseline{}, \rjBest{} more than doubles the
fraction of relevant columns retrieved.
Further, Semantic Relatedness measures defined over Wikipedia concepts account
for approximately 58\% of this improvement (over \rjAblateSR{}).

\begin{table}[h]
\centering
\scriptsize{
\begin{tabular}{|p{1.9cm}|p{1.5cm}|p{0.7cm}|p{1cm}|p{1.35cm}|}
\hline
\textbf{Model}  &   \textbf{cols added}    &   \textbf{acc.}    &   $\mathbf{DCG@4}$  &   $\mathbf{nDCG'@4}$\\ \hline

\rjBaseline{}   &    423    &   0.29    &   11.38   &   0.43    \\ \hline
\rjAblateSR &    414    &   0.43    &   13.71   &   0.48    \\ \hline
\rjBest{}   &    410    &   $\mathbf{0.62}$ &   $\mathbf{30.71}$    &   $\mathbf{0.76}$ \\  \hline
\end{tabular}
}
\caption{Results of comparison of three methods for the \columnRankingTaskName{} task. \rjBest{} more than doubles
the accuracy of added columns as compared to the baseline \rjBaseline{}. 
Using Semantic Relatedness feature results in a significantly improved performance in \rjBest{}
as compared to \rjAblateSR{}}
\label{table:comparison}
\end{table}




\subsubsection{Analysis}
Table \ref{tab:colRankCorrels} lists the correlation between features and user
rating for the \columnRankingTaskName{} task. Results show that columns
containing numeric data make more relevant additions to a table than other
non-numeric ones.
A greater value of Semantic Relatedness, more distinct values in the source
column, and a higher match percentage between matched columns leads to
higher-quality of added columns.

\begin{table}[h]
\centering
\scriptsize{
\begin{tabular}{|p{2.5cm}|p{3.5cm}|}
\hline
\textbf{Feature}    &   \textbf{Correlation with rating}    \\ \hline
tgtIsNum    &   0.285  \\ \hline
srRelate    &   0.253  \\  \hline
avgNumKeyMap    &   -0.231 \\  \hline
srcDistinctValFrac  &   0.209 \\  \hline
srcTableColIdx  &   -0.202   \\  \hline
matchPerc   &   0.167   \\  \hline
outLink &   -0.161  \\  \hline
inLink  &   -0.157   \\  \hline
srcNumericValFrac   &   0.134  \\  \hline
% srcAvgLen   &   -0.115 \\  \hline
% matchedTableColIdx  &   -0.073 \\  \hline
% tgtAvgLen   &   -0.033  \\  \hline
% tgtDistinctValFrac  &   0.033   \\  \hline
\end{tabular}
\caption{Spearman's rank correlation coefficient of \columnRankingTaskName{} features with column rating.
Only features with significant correlations $(p<0.01)$
are listed. Correlation is calculated over 485 pairs.}
\label{tab:colRankCorrels}
}
\end{table}

The results described above show that the \emph{Semantic Relatedness} measure
between Wikipedia pages is a very valuable indicator of relatedness of which
column should be added to a given table. A few examples illustrate this
relationship.
For a table of the top 10 largest power producing facilities, \rjAblateSR{} adds
an unrelated column containing the number of Olympic medals won by countries in
swimming. On the other hand, \rjBest{}, which uses the Semantic Relatedness
features, adds highly relevant information about the annual $CO_{2}$ emissions
of countries.
For another table, which contained the rankings of a popular Kylie Minogue song
across charts, \rjBest{} added the ranking of other songs by Kylie Minogue (highly relevant column),
whereas \rjAblateSR{} adds columns about songs of artists other than Kylie
Minogue.

These examples also support that fact that Semantic Relatedness between two pages is a very
important feature for the \columnRankingTaskName{} task. 

% Figure \ref{fig:srAdvantage} illustrates the significance of using the srRelate feature. Fig. \ref{fig:srAdvantage} $(a)$ shows
% a column added by \rjAblateSR{} and Fig. \ref{fig:srAdvantage} $(b)$ shows a column
% added to the same table by  \rjBest{}. It shows the usefulness of using the \emph{srRelate} feature. For the 
% table from the about one of Kylie Minogue's song, \rjAblateSR{} adds a column
% about the album ``Right Round'' which is a song, but not by Kylie Minogue. On the other hand, \rjBest{} adds 
% a column from about the album ``Love at First Sight'' which is a Kylie Minogue song. This shows that using the \emph{srRelate}
% feature adds columns that are more relevant to a given table.  

